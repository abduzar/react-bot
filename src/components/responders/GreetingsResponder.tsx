import React from 'react';

import { Text, useAnyEvent, useText } from '@urban-bot/core';

const esGreetings = [/[h,H]ola/];
const enGreetings = [/[h,H](ello|i|ey)/];

const greetings = [...esGreetings.values(), ...enGreetings.values()];

export function GreetingsResponder() {
    const [answer, setAnswer] = React.useState({ value: 'Let me introduce myself' });

    useAnyEvent(() => {
        console.log('calling after any type sending');
    });

    useText(({ from }) => {
        setAnswer({ value: `Hello ${from.firstName}` });
    }, greetings);

    return (
        <Text isNewMessageEveryRender simulateTyping={1000}>
            {answer.value}
        </Text>
    );
}
