import React from 'react';
import { GreetingsResponder } from './responders/GreetingsResponder';

export function Responder() {
    return (
        <>
            <GreetingsResponder />
        </>
    );
}
