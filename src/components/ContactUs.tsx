import React from 'react';
import { Contact } from '@urban-bot/core';

export function ContactUs() {
    return (
        <>
            <Contact firstName="Haykanush" lastName="Abelyan" phoneNumber="+37455534488" />
            <Contact firstName="Other" lastName="Person" phoneNumber="+79999999999" />
        </>
    );
}
