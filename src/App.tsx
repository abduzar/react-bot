import React from 'react';
import { Route, Text, Router } from '@urban-bot/core';
import { ContactUs } from './components/ContactUs';
import { Responder } from './components/MessageResponder';

export function App() {
    return (
        <>
            <Responder />
            <Text>
                Responding to commands
                <br />
                <br />
                /contact
            </Text>
            <Router>
                <Route path="/contact" description="Contact Our Team.">
                    <ContactUs />
                </Route>
            </Router>
        </>
    );
}
